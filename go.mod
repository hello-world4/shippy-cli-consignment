module gitlab.com/hello-world4/shippy-cli-consignment

go 1.15

replace google.golang.org/grpc => google.golang.org/grpc v1.26.0

require (
	github.com/micro/go-micro/v2 v2.9.1
	gitlab.com/hello-world4/shippy-service-consignment v1.0.11
	golang.org/x/sys v0.0.0-20201028094953-708e7fb298ac // indirect
	google.golang.org/genproto v0.0.0-20201028140639-c77dae4b0522 // indirect
	google.golang.org/grpc v1.33.1 // indirect
)
