package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	"context"

	micro "github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/metadata"
	pb "gitlab.com/hello-world4/shippy-service-consignment/proto/consignment"
)

const (
	address = "localhost:50051"
	defaultFilename = "consignment.json"
)

func parseFile(file string) (*pb.Consignment, error) {
	var consignmet *pb.Consignment
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	json.Unmarshal(data, &consignmet)
	return consignmet, err
}

func main() {
	service := micro.NewService(micro.Name("shippy.cli.consignment"))
	service.Init()

	client := pb.NewShippingService("shippy.service.consignment", service.Client())

	file := os.Getenv("FILE_NAME")
	token := os.Getenv("TOKEN")

	consignment, err := parseFile(file)

	if err != nil {
		log.Fatalf("Could not parse file: %v", err)
	}
	ctx := metadata.NewContext(context.Background(), map[string]string{
		"token": token,
	})

	response, err := client.CreateConsignment(ctx, consignment)
	if err != nil {
		log.Fatalf("Could not CreateConsignment: %v", err)
	}
	log.Printf("Created: %t", response.Created)

	consignments, err := client.GetConsignments(ctx, &pb.GetRequest{})
	if err != nil {
		log.Fatalf("Could not GetConsignments: %v", err)
	}
	log.Printf(consignments.String())
}